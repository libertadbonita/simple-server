FROM debian:jessie

# install java
RUN apt-get update &&\
    apt-get install -y git wget openjdk-7-jdk

# create user
RUN useradd server-user &&\
    mkdir -p /home/server-user &&\
    chown -R server-user /home/server-user 
USER server-user
WORKDIR /home/server-user

# install leiningen
RUN wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein &&\
    chmod a+x lein &&\
    ~/lein

# build server
RUN mkdir simple-server
COPY src simple-server/src
COPY project.clj simple-server/project.clj
WORKDIR simple-server
RUN ~/lein uberjar

# start server
ENTRYPOINT java -jar ~/simple-server/target/uberjar/*-standalone.jar

