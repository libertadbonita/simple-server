(ns simple-server.db
  (:require [clojure.java.jdbc :as j]))

(def pgsql-db {:dbtype "postgresql"
            :dbname "postgres"
            :host "postgres-db"
            :user "postgres"
            :password "danger"})

(defn retrieve-count
  []
  (first 
   (j/query pgsql-db
            ["select count from counter where counter_id = 0"]
            {:row-fn :count})))

(defn increment-count
  []
  (let [prev-count (retrieve-count)]
    (j/update! pgsql-db :counter
             {:count (inc prev-count)}
             ["counter_id = 0"])))
             
(defn create-table
  []
  (try
    (do
      (j/execute! pgsql-db
                [(j/create-table-ddl :counter
                                     [[:counter_id :int "not null"]
                                      [:count :int "not null"]])])
      (j/insert! pgsql-db :counter
                 {:counter_id 0
                  :count 0}))
    (catch org.postgresql.util.PSQLException e)))

