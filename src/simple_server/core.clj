(ns simple-server.core
  (:require [ring.adapter.jetty :as jetty]
            [simple-server.db :as db])
  (:gen-class))

(defn handle-post [request]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (str "Incremented to "
              (do
                (db/increment-count)
                (db/retrieve-count)))})

(defn handle-get [request]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (str "Current "
              (db/retrieve-count))})

(defn handle-not-found []
  {:status 404
   :headers {"Content-Type" "text/html"}
   :body "Not found"})

(defn handler [request]
  (let [method (:request-method request)]
    (cond
      (= method :post) (handle-post request)
      (= method :get) (handle-get request)
      :else (handle-not-found))))

(defn -main
  [& args]
  (db/create-table)
  (jetty/run-jetty handler {:port 3000}))
